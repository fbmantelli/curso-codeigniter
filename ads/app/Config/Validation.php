<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Validation\CreditCardRules;
use CodeIgniter\Validation\FileRules;
use CodeIgniter\Validation\FormatRules;
use CodeIgniter\Validation\Rules;

class Validation extends BaseConfig
{
    //--------------------------------------------------------------------
    // Setup
    //--------------------------------------------------------------------

    /**
     * Stores the classes that contain the
     * rules that are available.
     *
     * @var string[]
     */
    public $ruleSets = [
        Rules::class,
        FormatRules::class,
        FileRules::class,
        CreditCardRules::class,
    ];

    /**
     * Specifies the views that are used to display the
     * errors.
     *
     * @var array<string, string>
     */
    public $templates = [
        'list'   => 'CodeIgniter\Validation\Views\list',
        'single' => 'CodeIgniter\Validation\Views\single',
    ];

    //--------------------------------------------------------------------
    // Rules
    //--------------------------------------------------------------------
    
    //categories
    public $category = [
        'name'     => 'required|min_length[3]|max_length[90]|is_unique[categories.name,id,{id}]'
    ];

     public $category_errors = [
        'name' => [
            'required' => 'Categories.name.required', //lang() não pode ser colocado aqui por irá gerar erro
            'min_length' => 'Categories.name.min_length',
            'max_length' => 'Categories.name.max_length',
            'is_unique' => 'Categories.name.is_unique'
        ]
    ];

    //planos
    public $plan = [
        'name'        => 'required|min_length[3]|max_length[90]|is_unique[plans.name,id,{id}]',
        'recorrence'  => 'required|in_list[monthly,quarterly,semester,yearly]',
        'value'       => 'required',
        'description' => 'required'
    ];

     public $plan_errors = [
        'name' => [
            'required' => 'O nome do plano é obrigatório',
            'min_length' => 'O nome do plano deve possuir pelo menso 3 caracteres',
            'max_length' => 'O nome do plano deve possuir no máximo 90 caracteres',
            'is_unique' => 'Este nome do plano já existe'
        ],
        'recorrence' => [
            'required' => 'O recorrencia do plano é obrigatória',
            'in_list' => 'O recorrencia do plano deve ser um item da lista'
        ],
        'value' => [
            'required' => 'O valor do plano é obrigatório'
        ],
        'description' => [
            'required' => 'O descrição do plano é obrigatória'
        ]
    ];
 
}

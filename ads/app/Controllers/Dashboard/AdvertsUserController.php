<?php

namespace App\Controllers\Dashboard;

use App\Controllers\BaseController;
use App\Services\AdvertsService;
use CodeIgniter\Config\Factories;

class AdvertsUserController extends BaseController
{
    private $advertService;

    public function __construct()
    {
        $this->advertService = Factories::class(AdvertsService::class);
    }

    public function index()
    {
        $data = [
            'title' => 'Meus Anúncios'
        ];

        return view('Dashboard/Adverts/index', $data);
    }
}

<?php

namespace App\Database\Seeds;

use App\Models\PlanModel;
use CodeIgniter\Config\Factories;
use CodeIgniter\Database\Seeder;

class PlanSeeder extends Seeder
{
    public function run()
    {
        try {
            $this->db->transStart();
            $planModel = Factories::models(PlanModel::class);

            foreach (self::plans() as $plan) {
                $planModel->insert($plan);
            }

            $this->db->transComplete();
        } catch (\Throwable $th) {
            print $th;
        }
    }

    private static function plans():array {
        return [
            [
                'plan_id' => '9416',
                'name' => 'Plano Starter',
                'recorrence' => 'monthly',
                'description' => 'Plano básico inicial',
                'adverts' => '10',
                'value' => '19.90',
                'is_highlighted' => '0'
            ]
        ];
    }
}

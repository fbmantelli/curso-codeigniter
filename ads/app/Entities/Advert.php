<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class Advert extends Entity
{
    protected $dates   = ['created_at', 'updated_at', 'deleted_at', 'user_since'];
    protected $casts   = [
        'is_published'  => 'boolean',
        'adverts'       => '?integer',
        'display_phone' => 'boolean'
    ];


    public function setPrice(string $price) {
        $this->attributes['price'] = str_replace(',', '', $price);
    }

    public function setIsPublished(string $IsPublished) {
        $this->attributes['is_published'] = $IsPublished ? true : false;
    }

    public function recover() {
        $this->attributes['deleted_at'] = null;
    }

    public function unsetAuxiliaryAttributes() {
        //unset($this->attributes['address']);
        unset($this->attributes['images']);
    }

    public function image() {
        return 'Imagem';
    }

    public function isPublished() {
        return 'Publicado ou não';
    }

    public function address() {
        return 'O meu endereço aqui';
    }
}


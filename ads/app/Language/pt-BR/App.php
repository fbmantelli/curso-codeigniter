<?php

return [
    //messages

    'welcome' => 'Olá {nome}, bem vindo ao manager',
    'btn_action' => 'Ações',
    'btn_cancel' => 'Fechar',
    'btn_save' => 'Salvar',
    'btn_yes' => 'Sim',
    'btn_no' => 'Cancelar',
    'btn_back' => 'Voltar',
    'btn_archive' => 'Arquivar',
    'btn_all_archived' => 'Arquivados',
    'btn_recover' => 'Recuperar',
    'btn_delete' => 'Excluir',
    'btn_logout' => 'Encerrar sessão',
    'btn_edit' => 'Editar',


    'danger_validation' => 'Verifique os error e tente novamente.',
    'success_saved' => 'Dados salvos com sucesso!',
    'success_archived' => 'Arquivado com sucesso!',
    'success_recovered' => 'Recuperado com sucesso!',
    'success_deleted' => 'Excluído com sucesso!',
    'delete_confirmation' => 'Deseja realmente excluir?',
    'delete_info_confirmation' => 'Esta ação não poderá ser revertida.',
    'info_empty_data' => 'Não há dados para exibir',

    'sidebar' => [
        'manager' => [
            'home' => 'Principal',
            'categories' => 'Categorias'
        ],
    ],
];
<?php

return [
    'title_index' => 'Listando as categorias',
    'title_new' => 'Criar categoria',
    'title_edit' => 'Editar categoria',
    'title_archived' => 'Categorias arquivadas',

    'label_name' => 'Nome',
    'label_choose_category' => 'Escolha uma categoria...',
    'label_slug' => 'Slug',
    'label_parent_name' => 'Categoria pai',
    'label_categories_archived' => 'Categorias arquivadas',

    //validações
    'name' => [
        'required' => 'O nome é obrigatório.',
        'min_length' => 'Informe pelo menos 3 caracteres para o nome',
        'max_length' => 'Informe no máximo 90 caracteres para o nome',
        'is_unique' => 'Esta categoria já existe'
    ],
];
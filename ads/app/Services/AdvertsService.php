<?php
namespace App\Services;

use App\Models\AdvertModel;
use CodeIgniter\Config\Factories;

class AdvertsService {

    private $user;
    private $advertModel;

    public const SITUATION_NEW  = 'new';
    public const SITUATION_USED = 'used';

    public function __construct()
    {
        $this->user = service('auth')->user();
        $this->advertModel = Factories::models(AdvertModel::class);
    }

    public function getAllAdverts(
        bool $showBtnArchive = true,
        bool $showBtnViewAdvert = true,
        bool $showBtnQuestions = true,
        string $classBtnActions = 'btn btn-primary, btn-sm',
        string $sizeImage = 'small'
    ): array {
        
        $adverts = $this->advertModel->getAllAdverts();
        
        $baseRouteToEditImages = $this->user->isSuperadmin() ? 'adverts.manager.edit.images' : 'adverts.my.edit.images';
        $baseRouteToQuestions  = $this->user->isSuperadmin() ? 'adverts.manager.edit.questions' : 'adverts.my.edit.questions';

        $data = [];

        foreach($adverts as $advert) {

            $btnEdit = form_button([
                    'data-id' => $advert->id,
                    'id' => 'BtnEditAdvert',
                    'class' => 'dropdown-item'
                ],
                lang('App.btn_edit')
            );

            $finalRouteToEditImages = route_to($baseRouteToEditImages, $advert->id);
            $btnEditImages = form_button([
                    'class' => 'dropdown-item',
                    'onClick' => "location.href = '{$finalRouteToEditImages}'"
                ],
                'Editar imagens'
            );

            //montagem botão de ações dropdown
            $btnActions  = '<div class="dropdown dropup">';

            $attrBtnActions = [
                'type'  => 'button',
                'id'    => 'actions',
                'class' => "dropdown-toggle {$classBtnActions}",
                'data-bs-toggle' => 'dropdown',
                'data-toggle'    => 'dropdown',
                'aria-haspopup'  => 'true',
                'aria-expanded'  => 'false'
            ];

            $btnActions .= form_button($attrBtnActions, 'Ações');
            $btnActions .= '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
            $btnActions .= $btnEdit;
            $btnActions .= $btnEditImages;
            $btnActions .= '</div>';
            $btnActions .= '</div>';

            $data[] = [
                'image' => $advert->image(),
                'title' => $advert->title,
                'code' => $advert->code,
                'category' => $advert->category,
                'is_published' => $advert->isPublished(),
                'address' => $advert->address(),
                'actions' => $btnActions,
            ];
        }

        return $data;
    }

}
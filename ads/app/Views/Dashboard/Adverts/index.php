<?php $this->extend('Dashboard/Layout/main'); ?>

<?php $this->section('title'); ?>
<?php echo $title ?? ''; ?>
<?php $this->endSection(); ?>

<?php $this->section('styles'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/r-2.3.0/datatables.min.css" />
<?php $this->endSection(); ?>


<?php $this->section('content'); ?>
<section class="dashboard section">
	<!-- Container Start -->
	<div class="container">
		<!-- Row Start -->
		<div class="row">

			<?php echo $this->include('Dashboard/Layout/_sidebar'); ?>

			<div class="col-md-10 offset-md-1 col-lg-8 offset-lg-0">
				<!-- Recently Favorited -->
				<div class="widget dashboard-container my-adslist">
					<h3 class="widget-header">Lista dos meus anúncios</h3>
					<div class="row mb-4">
						<div class="col-md-12">
							<button type="button" class="btn btn-main-sm add-button mb-2 float-right"><i class="fa fa-plus"></i> Criar</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							

							<table class="table table-striped table-bordless" id="dataTable">
								<thead>
									<tr>
										<th scope="col">Imagem</th>
										<th scope="col" class="none">Código</th>
										<th scope="col" class="all">Título</th>
										<th scope="col" class="none text-center">Categoria</th>
										<th scope="col">Status</th>
										<th scope="col" class="none">Endereço</th>
										<th scope="col" class="all text-center"><?php echo lang('App.btn_action'); ?></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row End -->
	</div>
	<!-- Container End -->
</section>
<?php $this->endSection(); ?>

<?php $this->section('scripts'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/r-2.3.0/datatables.min.js"></script>

<?php echo $this->include('Dashboard/Adverts/Scripts/_datatable_all'); ?>

<script>
	function refreshCSRFToken(token) {
		$('[name="<?php echo csrf_token(); ?>"]').val(token);
		$('[meta="<?php echo csrf_token(); ?>"]').attr('content', token);
	}
</script>
<?php $this->endSection(); ?>
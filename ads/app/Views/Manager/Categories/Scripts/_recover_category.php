<script type="text/javascript">
$(document).on('click', '#recoverCategoryBtn', function() {
    var id = $(this).data('id');

    var url = '<?php echo route_to('categories.recover') ?>';
    var tk = $('meta[name="<?php echo csrf_token(); ?>"]').attr('content');
    var data =  {
            '<?php echo csrf_token(); ?>': tk,
            _method: 'PUT',
            id: id
        }

    $.post(url, {
            '<?php echo csrf_token(); ?>': tk,
            _method: 'PUT',
            id: id
        },
        function(response){
            window.refreshCSRFToken(response.token);
            toastr.success(response.message);
            $('#dataTable').DataTable().ajax.reload(null, false);
    }, 'json').fail(function() {
        toastr.error('Error backend.');
    });
 
});
</script>
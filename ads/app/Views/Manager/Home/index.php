<?php $this->extend('Manager/Layout/main'); ?>

<?php $this->section('title'); ?>
<?php echo $title ?? ''; ?>
<?php $this->endSection(); ?>

<?php $this->section('styles'); ?>
<?php $this->endSection(); ?>


<?php $this->section('content'); ?>
    <div class="container-fluid">
        <h1 class="mt-4"><?php echo $title ?? '';  ?></h1>
    </div>
<?php $this->endSection(); ?>

<?php $this->section('scripts'); ?>
<?php $this->endSection(); ?>
<script type="text/javascript">
$(document).on('click', '#deletePlanBtn', function(e) {
    e.preventDefault();
    var id = $(this).data('id');

    var url = '<?php echo route_to('plans.delete') ?>';
    var tk = $('meta[name="<?php echo csrf_token(); ?>"]').attr('content');

    Swal.fire({
            title: '<?php echo lang('App.delete_confirmation'); ?>',
            text: "<?php echo lang('App.delete_info_confirmation'); ?>",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '<?php echo lang('App.btn_yes'); ?>',
            cancelButtonText: '<?php echo lang('App.btn_no'); ?>'
        }).then((result) => {
        if (result.isConfirmed) {
            $.post(url, {
                    '<?php echo csrf_token(); ?>': tk,
                    _method: 'DELETE',
                    id: id
                },
                function(response){
                    window.refreshCSRFToken(response.token);
                    toastr.success(response.message);
                    $('#dataTable').DataTable().ajax.reload(null, false);
            }, 'json').fail(function() {
                toastr.error('Error backend.');
            });
        }
    })
});
</script>
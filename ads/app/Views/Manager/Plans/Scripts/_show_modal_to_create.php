<script type="text/javascript">
$(document).on('click', '#createPlanBtn', function() {

    $('.modal-title').text('Criar Plano');
    $('#planModal').modal('show');
    $('input[name="id"]').val('');
    $('input[name="_method"]').remove();
    $("#plans-form")[0].reset();
    $('#plans-form').attr('action', '<?php echo route_to('plans.create'); ?>');
    $('#plans-form').find('span.error-text').text('');

    var url = '<?php echo route_to('plans.get.recorrences') ?>';

    $.get(url, function(response){
        $('#boxRecorrences').html(response.recorrences);        
    }, 'json');

});
</script>
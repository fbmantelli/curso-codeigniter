<script type="text/javascript">
$('#plans-form').submit(function(e) {
    e.preventDefault();
    var form = this;

    $.ajax({
        url: $(form).attr('action'),
        type: $(form).attr('method'),
        data: new FormData(form),
        processData: false,
        dataType: 'JSON',
        contentType: false,
        beforeSend: function() {
            $(form).find('span.error-text').text('');
        },
        success: function(response) {
            window.refreshCSRFToken(response.token);

            if (response.success == false) {
               
                toastr.error('Verifique os erros e tente novamente.');

                $.each(response.errors, function(field, value) {
                    $(form).find('span.' + field).text(value);
                });

                return;
            }

            toastr.success(response.message);

            $('#planModal').modal('hide');
            $(form)[0].reset();

            $('#dataTable').DataTable().ajax.reload(null, false);
            $('.modal-title').text('Criar Plano');
            $(form).attr('action', '<?php echo route_to('plans.create'); ?>');
            $(form).find('input[name="id"]').val('');
            $('input[name="_method"]').remove();
        },
        error: function(request, status, error) {
            toastr.error('Error backend.');
        }
    });
});
</script>
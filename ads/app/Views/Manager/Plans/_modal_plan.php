<div class="modal fade" id="planModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Novo Plano</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <?php echo form_open(route_to('plans.create'), ['id' => 'plans-form', 'method' => 'post'], ['id' => '']); ?>
                <div class="modal-body">
                    <div class="row">
                        <div class="mb-3">
                            <label for="name" class="form-label">Nome do plano</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <span class="text-danger error-test name"></span>
                        </div>
                        <div class="mb-3">
                            <label for="recorrence" class="form-label">Recorrência do plano</label>
                            <span id="boxRecorrences"></span>
                            <span class="text-danger error-text recorrence"></span>
                        </div>
                        <div class="mb-3">
                            <label for="value" class="form-label">Valor</label>
                            <input type="text" class="money form-control" id="value" name="value">
                            <span class="text-danger error-test value"></span>
                        </div>
                        <div class="mb-3">
                            <label for="adverts" class="form-label">Nº de anúncios permitidos</label>
                            <input type="number" class="form-control" id="adverts" name="adverts">
                            <small>Nº de anúncios que o usuário poderá cadastrar. Deixe em branco para anúncios ilimitados.</small>
                            <span class="text-danger error-test adverts"></span>
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Descrição do plano</label>
                            <textarea class="form-control" id="description" name="description" rows="3" placeholder="Descrição"></textarea>
                            <span class="text-danger error-test description"></span>
                        </div>
                    </div>

                    <div class="form-check form-switch">
                        <?php echo form_hidden('is_highlighted', 0); ?>
                        <input type="checkbox" class="form-check-input" id="is_highlighted" name="is_highlighted">
                        <label for="is_highlighted" class="form-check-label">Destacar plano no site</label>
                    </div>                    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" id= "btnSubmit" class="btn btn-secondary btn-sm" data-bs-dismiss="modal"><?php echo lang('App.btn_cancel'); ?></button>
                    <button type="submit" class="btn btn-primary btn-sm"><?php echo lang('App.btn_save'); ?></button>
                </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</div>
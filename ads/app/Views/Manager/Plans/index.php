<?php $this->extend('Manager/Layout/main'); ?>

<?php $this->section('title'); ?>
<?php echo $title; ?>
<?php $this->endSection(); ?>

<?php $this->section('styles'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/r-2.3.0/datatables.min.css"/>
<?php $this->endSection(); ?>


<?php $this->section('content'); ?>
    <div class="container-fluid pt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mt-4"><?php echo $title; ?>
                            <button id="createPlanBtn" class="btn btn-success btn-sm float-end">Novo plano</button>
                        </h5>
                    </div>
                    <div class="card-body">

                        <a href="<?php echo route_to('plans.archived') ?>" class="btn btn-info btn-sm mt-2 mb-2">Arquivados</a>
                        <table class="table table-striped table-bordless" id="dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">Código</th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Destaque?</th>
                                    <th scope="col">Detalhes</th>
                                    <th scope="col"><?php echo lang('App.btn_action'); ?></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <?php echo $this->include('Manager/Plans/_modal_plan'); ?>
    
    
<?php $this->endSection(); ?>

<?php $this->section('scripts'); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/r-2.3.0/datatables.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('manager_assets/mask/jquery.mask.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo site_url('manager_assets/mask/app.js'); ?>"></script>

<?php echo $this->include('Manager/Plans/Scripts/_datatable_all'); ?>
<?php echo $this->include('Manager/Plans/Scripts/_show_modal_to_create'); ?>
<?php echo $this->include('Manager/Plans/Scripts/_submit_modal_create_update'); ?>
<?php echo $this->include('Manager/Plans/Scripts/_get_plan_info'); ?>
<?php echo $this->include('Manager/Plans/Scripts/_archive_plan'); ?>

<script>
    function refreshCSRFToken(token) {
        $('[name="<?php echo csrf_token(); ?>"]').val(token);
        $('[meta="<?php echo csrf_token(); ?>"]').attr('content', token);
    }
</script>
<?php $this->endSection(); ?>